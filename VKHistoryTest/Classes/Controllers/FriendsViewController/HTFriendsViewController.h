//
//  HTFriendsViewController.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTFriendsViewController : UIViewController

@property (nonatomic, retain) NSString *userId;

@end
