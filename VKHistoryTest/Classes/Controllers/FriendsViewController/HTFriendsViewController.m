//
//  HTFriendsViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTFriendsViewController.h"

#import <VKSdk.h>
#import "HTNetworkManager.h"
#import "HTUser.h"
#import "HTFriendsTableViewCell.h"

@interface HTFriendsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HTUser *user;

@end

@implementation HTFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationItem] setHidesBackButton:YES animated:NO];
    [[self tableView] setDelegate:self];
    [[self tableView] setDataSource:self];
    
    __block typeof(self) blockSelf = self;
    
    [[HTNetworkManager sharedManager] getFriendsWithUserId:[self userId]
                                                     count:0
                                                    offset:0
                                                 onSuccess:^(id friends)
                                                    {
                                                        [blockSelf setUser:friends[NSStringFromClass([HTUser class])]];
                                                        [[blockSelf tableView] reloadData];
                                                    }
                                                 onFailure:^(NSError *error)
                                                    {
         
                                                    }];
    [[self tableView] registerNib:[UINib nibWithNibName:@"HTFriendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"HTFriendsTableViewCell"];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;//[[[self user] friends] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *categoryIdentifier = @"HTFriendsTableViewCell";
    HTFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:categoryIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    
//    NSString *firstName = [[[[self user] friends] objectAtIndex:[indexPath row]] firstName];
//    NSString *lastName = [[[[self user] friends] objectAtIndex:[indexPath row]] lastName];
//    NSURL *photoUrlName = [[[[self user] friends] objectAtIndex:[indexPath row]] photoUrl];
//    NSInteger userId = [[[[self user] friends] objectAtIndex:[indexPath row]] userId];
    
//    [cell performCellWithUserName:[firstName copy]
//                         lastName:[lastName copy]
//                        avatarUrl:[photoUrlName retain]
//                           userId:userId];
//    
//    [firstName release];
//    [lastName release];
//    [photoUrlName release];
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)logoutAction:(UIBarButtonItem *)sender
{
    [VKSdk forceLogout];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    [[self navigationController] popViewControllerAnimated:NO];
}

#pragma mark - 
#pragma mark - Dealloc

- (void)dealloc
{
    [_tableView release];
    [_user release];
    [_userId release];
    [super dealloc];
}
@end
