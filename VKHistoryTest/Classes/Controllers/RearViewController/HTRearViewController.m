//
//  HTRearViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTRearViewController.h"
#import "SWRevealViewController.h"
#import "HTProfileViewController.h"
#import "HTNewsViewController.h"
#import "HTNavigationController.h"
#import "HTSearchVideoViewController.h"
#import "HTRearTableViewCell.h"

@interface HTRearViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger _presentedRow;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@end

@implementation HTRearViewController

#pragma mark -
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Menu"];
    [[self tableView] setEstimatedRowHeight:60.0];
    [[self tableView] setRowHeight:UITableViewAutomaticDimension];
    [[self tableView] registerNib:[UINib nibWithNibName:@"HTRearTableViewCell" bundle:nil] forCellReuseIdentifier:@"HTRearTableViewCell"];
}

#pragma mark -
#pragma marl - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HTRearTableViewCell";
    HTRearTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSString *text = nil;
    NSString *imageName = nil;
    if ([indexPath row] == 0)
    {
        text = @"News";
        imageName = @"News-50";
    }
    else if ([indexPath row] == 1)
    {
        text = @"Profile";
        imageName = @"User-50";
    }
    else if ([indexPath row] == 2)
    {
        text = @"Video";
        imageName = @"Showing50";
    }
    else if ([indexPath row] == 3)
    {
        text = @"Logout";
        imageName = @"Logout64";
    }
    [cell performCellWithImageString:[imageName retain]
                        nameCategory:[text retain]];
    
    [imageName release];
    [text release];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = [self revealViewController];

    if ([indexPath row] == _presentedRow )
    {
        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        return;
    }
        
    UIViewController *newFrontController = nil;
    
    if ([indexPath row] == 0)
    {
        newFrontController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([HTNewsViewController class])];
    }
    else if ([indexPath row] == 1)
    {
        newFrontController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([HTProfileViewController class])];
    }
    else if ([indexPath row] == 2)
    {
        newFrontController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([HTSearchVideoViewController class])];
    }
    
    HTNavigationController *navigationController = [[[HTNavigationController alloc] initWithRootViewController:newFrontController] autorelease];
    [revealController pushFrontViewController:navigationController animated:YES];
    
    _presentedRow = [indexPath row];  // <- store the presented row
}

- (__kindof UIViewController *)assemblyViewControllerWithWithIdentifier:(NSString *)identifier
{
    return [[self assemblyStoryboard] instantiateViewControllerWithIdentifier:identifier];
}

- (UIStoryboard *)assemblyStoryboard
{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

@end
