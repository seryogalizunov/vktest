//
//  HTProfileViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/14/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTProfileViewController.h"
#import "HTDataManager.h"
#import "HTProfile.h"
#import "HTPhotoAlbum.h"
#import "HTPhoto.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface HTProfileViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (retain, nonatomic) IBOutlet UILabel *userNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *dataBirthdayLabel;
@property (retain, nonatomic) IBOutlet UILabel *cityLabel;
@property (retain, nonatomic) IBOutlet UILabel *schoolLabel;
@property (retain, nonatomic) IBOutlet UILabel *universityLabel;

@property (retain, nonatomic) IBOutlet UIImageView *photoImageView;
@property (retain, nonatomic) IBOutlet UIStackView *userInfoStackView;
@property (retain, nonatomic) IBOutlet UIStackView *userPhotoStackView;

@property (nonatomic, strong) NSMutableArray *photosUserArray;

@end

@implementation HTProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setPhotosUserArray:[[[NSMutableArray alloc] init] autorelease]];
    [self performGetUserRequest];
}

- (void)performGetUserRequest
{
    __block typeof(self) blockSelf = self;
    [[HTDataManager sharedManager] getUserProfilesWithAccessToken:[self accessToken]
                                                           userId:[self userId]
                                                        onSuccess:^(id responseObject)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [blockSelf performSetInfoProfile:responseObject[NSStringFromClass([HTProfile class])]];
                                                            });
                                                        }
                                                        onFailure:^(NSError *error)
                                                        {
                                                            
                                                        }];
    
    [[HTDataManager sharedManager] getUserPhotosWithAccessToken:[self accessToken]
                                                         userId:[self userId]
                                                      onSuccess:^(id responseObject)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [blockSelf performSetUserPhotos:responseObject[NSStringFromClass([HTPhotoAlbum class])]];
                                                            });
                                                        }
                                                      onFailure:^(NSError *error)
                                                        {
                                                             
                                                        }];
}

#pragma mark -
#pragma amrk - performSetInfoProfile

- (void)performSetInfoProfile:(HTProfile *)profile
{
    [[self userNameLabel] setText:[NSString stringWithFormat:@"%@ %@", [profile firstName], [profile lastName]]];
    [[self dataBirthdayLabel] setText:[profile dateOfBirth]];
    [[self cityLabel] setText:@"Харьков"];
    [[self schoolLabel] setText:[profile school]];
    [[self universityLabel] setText:[profile university]];
    [[self photoImageView] pin_setImageFromURL:[NSURL URLWithString:[self getStringWithUrl:[profile photoUrl]]]];
    
    [[[self photoImageView] layer] setMasksToBounds:YES];
    [[[self photoImageView] layer] setCornerRadius:(CGRectGetWidth([[self photoImageView] frame]) / 2)];
}

#pragma mark - 
#pragma mark - performSetUserPhotos

- (void)performSetUserPhotos:(HTPhotoAlbum *)userPhoto
{
    if ([[userPhoto photosArray] count] > 0)
    {
        for (NSInteger index = 0; index < 3; index++)
        {
            UIImageView *imageView = [[[UIImageView alloc] init] autorelease];
            [imageView pin_setImageFromURL:[NSURL URLWithString:[self getStringWithUrl:[[[userPhoto photosArray] objectAtIndex:index] photoUrl]]]];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
            [[self userPhotoStackView] addArrangedSubview:[imageView retain]];
            [[self photosUserArray] addObject:[imageView retain]];
            __block typeof(self) blockSelf = self;
            [UIView animateWithDuration:0
                             animations:^{
                                 [[[imageView heightAnchor] constraintEqualToConstant:(CGRectGetWidth([[blockSelf userPhotoStackView] frame]) / 3)] setActive:YES];
                                 [[imageView layer] setCornerRadius:CGRectGetHeight([[blockSelf userPhotoStackView] frame]) / 2];
                                 [[blockSelf userPhotoStackView] layoutIfNeeded];
                             }];
            [imageView release];
            [imageView release];
        }
    }
}

#pragma mark - 
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage* image = [[info valueForKey:UIImagePickerControllerEditedImage] retain];
    NSString *stringPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Photos"];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:stringPath
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
    }
    NSString* imageName = [NSString stringWithFormat:@"%@.jpg",[self randomStringOfLenght:10]];
    NSString *fileName = [stringPath stringByAppendingString:[NSString stringWithFormat:@"/%@",imageName]];
    NSData *data = UIImageJPEGRepresentation(image, 0.2);
    [data writeToFile:fileName
           atomically:YES];
    
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] retain];
    
    [[self photosUserArray] addObject:[imageView retain]];
    [[self userPhotoStackView] addArrangedSubview:imageView];
    
    [self performStackViewSetUserPhotosArray:[self photosUserArray]];
    
    [imageView release];
    [imageView release];
    [imageView release];
    [image release];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)performStackViewSetUserPhotosArray:(NSArray *)photosArray
{
    for (NSInteger index = 0; index < [photosArray count]; index++)
    {
        UIImageView *imageView = [photosArray objectAtIndex:index];
        [imageView setContentMode:UIViewContentModeRedraw];
        [[imageView layer] setMasksToBounds:YES];
        
        __block typeof(self) blockSelf = self;
        [UIView animateWithDuration:0
                         animations:^{
                             [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
                             [imageView removeConstraints:[imageView constraints]];
                             [[[imageView heightAnchor] constraintEqualToConstant:(CGRectGetWidth([[blockSelf userPhotoStackView] frame]) / [photosArray count])] setActive:YES];
                             [[imageView layer] setCornerRadius:(CGRectGetWidth([[blockSelf userPhotoStackView] frame]) / [photosArray count]) / 2];
                             [[blockSelf userPhotoStackView] layoutSubviews];
                         }];
    }
}

#pragma mark - 
#pragma mark - Actions Methods

- (IBAction)addPhotoBarButtonAction:(UIBarButtonItem *)sender
{
    UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
    [pickerView setAllowsEditing:YES];
    [pickerView setDelegate:self];
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:true completion:nil];
}

#pragma mark -
#pragma mark - Private Methods

//- (UIImageView *)createImageViewWithImage:(UIImage *)image
//{
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//    [imageView setContentMode:UIViewContentModeScaleToFill];
//    [[imageView layer] setMasksToBounds:YES];
//    [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
//    return imageView;
//}

- (NSString *)getStringWithUrl:(NSURL *)url
{
    return [NSString stringWithFormat:@"%@", url];
}

- (NSString *)randomStringOfLenght:(long)lenght
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSMutableString* randomString = [NSMutableString stringWithCapacity:lenght];
    
    for (long i = 0; i < lenght; i++)
    {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random_uniform((int)[letters length])]];
    }
    NSString *string = [NSString stringWithFormat:@"%@%f",randomString, timeStamp];
    string = [string stringByReplacingOccurrencesOfString:@"."
                                               withString:@""];
    return string;
}

#pragma mark - 
#pragma mark - Dealloc

- (void)dealloc
{
    [_userNameLabel release];
    [_dataBirthdayLabel release];
    [_cityLabel release];
    [_schoolLabel release];
    [_universityLabel release];
    [_photoImageView release];
    [_userInfoStackView release];
    [_userPhotoStackView release];
    [super dealloc];
}

@end
