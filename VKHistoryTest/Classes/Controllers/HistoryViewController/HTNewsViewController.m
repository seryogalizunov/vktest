//
//  HTHistoryViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/9/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTNewsViewController.h"
#import <VKSdk.h>
#import "HTDataManager.h"
#import "HTUser.h"
#import "HTNewsTableViewCell.h"
#import "HTNews.H"
#import "HTLikes.h"
#import "HTRepost.h"
#import "HTProfileViewController.h"
#import "SWRevealViewController.h"

@interface HTNewsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger numberSection;
@property (nonatomic, strong) NSMutableArray <HTUser *> *userNewsArray;
@property (nonatomic, assign) BOOL isLoadNews;

@end

@implementation HTNewsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNumberSection:1];
    [self setIsLoadNews:NO];
    [self setUserNewsArray:[[[NSMutableArray alloc] init] autorelease]];
    
    [self performSetTableView];
    [self performGetNewsRequest];
}

#pragma mark - 
#pragma mark - PerformSetTableView

- (void)performSetTableView
{
    [[self tableView] setDelegate:self];
    [[self tableView] setDataSource:self];
    [[self tableView] setEstimatedRowHeight:44.0];
    [[self tableView] setRowHeight:UITableViewAutomaticDimension];
    [[self tableView] registerNib:[UINib nibWithNibName:@"HTNewsTableViewCell" bundle:nil] forCellReuseIdentifier:@"HTNewsTableViewCell"];
}

#pragma mark - 
#pragma mark - PerformGetNewsRequest

- (void)performGetNewsRequest
{
    __block typeof(self) blockSelf = self;
    [[HTDataManager sharedManager] getNewsFeedWithAccessToken:[self accessToken]
                                                  itemsOffset:@""
                                                    onSuccess:^(id responseObject)
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            HTUser *userModel = responseObject[NSStringFromClass([HTUser class])];
                                                            [[blockSelf userNewsArray] addObject:[userModel retain]];
                                                            [userModel release];
                                                            [[blockSelf tableView] reloadData];
                                                        });
                                                    }
                                                    onFailure:^(NSError *error)
                                                    {
         
                                                    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self numberSection];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self userNewsArray] count] == 0)
    {
        return 0;
    }
    else
    {
        return [[[[self userNewsArray] objectAtIndex:section] newsFeed] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *newsIdentifier = @"HTNewsTableViewCell";
    
    HTNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsIdentifier];
    [cell performCellWithNews:[[[[[self userNewsArray] retain] objectAtIndex:[indexPath section]] newsFeed] objectAtIndex:[indexPath row]]];
    [[self userNewsArray] release];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ([[[[self userNewsArray] objectAtIndex:[indexPath section]] newsFeed] count] - 1) && [indexPath section] == ([[self userNewsArray] count] - 1) && [self isLoadNews] == NO)
    {
        [self setIsLoadNews:YES];
        [self showHud];
        NSLog(@"NewFeed");
        __block typeof(self) blockSelf = self;
        [[HTDataManager sharedManager] getNewsFeedWithAccessToken:[self accessToken]
                                                      itemsOffset:[[[self userNewsArray] lastObject] itemsOffset]
                                                        onSuccess:^(id responseObject)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [blockSelf hideHud];
                                                                [blockSelf setIsLoadNews:NO];
                                                                HTUser *userModel = responseObject[NSStringFromClass([HTUser class])];
                                                                [[blockSelf userNewsArray] addObject:[userModel retain]];
                                                                [userModel release];
                                                                blockSelf.numberSection++;
                                                                [[blockSelf tableView] reloadData];
                                                            });
                                                        }
                                                        onFailure:^(NSError *error)
                                                        {
             
                                                        }];
    }
}

#pragma mark -
#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    HTProfileViewController *profilesViewController = (HTProfileViewController *)[segue destinationViewController];
    [profilesViewController setAccessToken:[self accessToken]];
    [profilesViewController setUserId:[self userId]];
}

#pragma mark -
#pragma mark - Dealloc

- (void)dealloc
{
    [_tableView release];
    [super dealloc];
}

@end
