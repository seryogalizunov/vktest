//
//  HTFriendsTableViewCell.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTFriendsTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface HTFriendsTableViewCell ()

@property (retain, nonatomic) IBOutlet UIImageView *photoImageView;
@property (retain, nonatomic) IBOutlet UILabel *userName;
@property (retain, nonatomic) IBOutlet UILabel *userId;


@end

@implementation HTFriendsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[[self photoImageView] layer] setCornerRadius:25];
}

- (void)performCellWithUserName:(NSString *)firstName
                       lastName:(NSString *)lastName
                      avatarUrl:(NSURL *)avatarUrl
                         userId:(NSInteger)userId
{
    [[self userName] setText:[NSString stringWithFormat:@"%@ %@", firstName, lastName]];
    [[self userId] setText:[NSString stringWithFormat:@"%ld", (long)userId]];
    [[self photoImageView] pin_setImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", avatarUrl]]];
}

- (void)dealloc
{
    [_photoImageView release];
    [_userName release];
    [_userId release];
    [super dealloc];
}
@end
