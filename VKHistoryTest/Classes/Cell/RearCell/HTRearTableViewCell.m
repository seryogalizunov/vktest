//
//  HTRearTableViewCell.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTRearTableViewCell.h"

@interface HTRearTableViewCell ()

@property (retain, nonatomic) IBOutlet UIImageView *imageImageView;
@property (retain, nonatomic) IBOutlet UILabel *nameCategoryLabel;

@end

@implementation HTRearTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)performCellWithImageString:(NSString *)imageString
                      nameCategory:(NSString *)nameCategory
{
    [[self imageImageView] setImage:[UIImage imageNamed:imageString]];
    [[self nameCategoryLabel] setText:nameCategory];
}

- (void)dealloc
{
    [_imageImageView release];
    [_nameCategoryLabel release];
    [super dealloc];
}
@end
