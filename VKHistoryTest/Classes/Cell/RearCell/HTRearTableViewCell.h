//
//  HTRearTableViewCell.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTRearTableViewCell : UITableViewCell

- (void)performCellWithImageString:(NSString *)imageString
                      nameCategory:(NSString *)nameCategory;

@end
