//
//  HTBaseViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/21/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTBaseViewController.h"
#import "SWRevealViewController.h"

@interface HTBaseViewController ()

@end

@implementation HTBaseViewController

@synthesize accessToken;
@synthesize userId;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performRevealViewController];
    [self performSetUserIdAndAccessToken];
}

#pragma mark - 
#pragma mark - performRevealViewController

- (void)performRevealViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:revealController
                                                                        action:@selector(revealToggle:)];
    
    [revealButtonItem setTintColor:[UIColor whiteColor]];
    [[self navigationItem] setLeftBarButtonItem:revealButtonItem];
}

#pragma mark - 
#pragma mark - performSetUserIdAndAccessToken

- (void)performSetUserIdAndAccessToken
{
    [self setAccessToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"accessToken"]];
    [self setUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
}

- (__kindof UIViewController *)assemblyViewControllerWithWithIdentifier:(NSString *)identifier
{
    return [[self assemblyStoryboard] instantiateViewControllerWithIdentifier:identifier];
}

- (UIStoryboard *)assemblyStoryboard
{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

#pragma mark - 
#pragma mark - Show/Hide Hud

- (void)showHud
{
    [MBProgressHUD showHUDAddedTo:self.view
                         animated:YES];
}

- (void)hideHud
{
    [MBProgressHUD hideHUDForView:self.view
                         animated:YES];
}

#pragma mark - 
#pragma mark - Dealloc

- (void)dealloc
{
    [accessToken release];
    [userId release];
    [super dealloc];
}

@end
