//
//  NSString+Category.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

- (NSString *)stringByStrippingHTML;
- (NSString *)timeFormatted:(int)totalSeconds;

@end
