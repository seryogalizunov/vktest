//
//  NSString+Category.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "NSString+Category.h"

@implementation NSString (Category)

- (NSString *)stringByStrippingHTML
{
    NSRange r;
    NSString *s = [[self copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

@end
