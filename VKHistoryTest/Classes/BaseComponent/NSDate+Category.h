//
//  NSDate+Category.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Category)

- (NSString *)converToString;

@end
