//
//  HTSerchVideoModel.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTSearchVideoModel.h"
#import "HTVideoModel.h"

@interface HTSearchVideoModel ()

@property (nonatomic, copy, readwrite) NSArray <HTVideoModel *> *videoArray;

@end

@implementation HTSearchVideoModel

- (instancetype)initWithVideoArray:(NSArray<HTVideoModel *> *)videoArray
{
    self = [super init];
    if (self)
    {
        [self setVideoArray:[videoArray retain]];
        [videoArray release];
    }
    return self;
}

@end
