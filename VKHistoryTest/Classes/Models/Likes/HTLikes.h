//
//  HTLikes.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTLikes : NSObject

@property (nonatomic, assign, readonly) NSInteger countLikes;
@property (nonatomic, assign, readonly) BOOL userIsLikes;

- (instancetype)initWithCountLikes:(NSInteger)count
                       userIsLikes:(BOOL)isLikes;

@end
