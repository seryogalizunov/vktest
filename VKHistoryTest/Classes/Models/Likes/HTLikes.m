//
//  HTLikes.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTLikes.h"

@interface HTLikes ()

@property (nonatomic, assign, readwrite) NSInteger countLikes;
@property (nonatomic, assign, readwrite) BOOL userIsLikes;

@end

@implementation HTLikes

- (instancetype)initWithCountLikes:(NSInteger)count
                       userIsLikes:(BOOL)isLikes
{
    self = [super init];
    if (self)
    {
        [self setCountLikes:count];
        [self setUserIsLikes:isLikes];
    }
    return self;
}

@end
