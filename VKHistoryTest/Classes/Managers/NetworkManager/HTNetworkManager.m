//
//  HTNetworkManager.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/9/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTNetworkManager.h"
#import <AFNetworking/AFNetworking.h>
#import "HTUser.h"
#import "HTFriendInfo.h"
#import "HTNews.h"
#import "HTLikes.h"
#import "NSString+Category.h"
#import "HTRepost.h"
#import "HTProfile.h"
#import "HTPhotoAlbum.h"
#import "HTPhoto.h"
#import "HTVideoModel.h"
#import "HTSearchVideoModel.h"
#import <VK-ios-sdk/VKSdk.h>
#import "NSDate+Category.h"

@interface HTNetworkManager ()

@property (nonatomic, strong) AFHTTPSessionManager *requestOperationManager;
@property (nonatomic, strong) dispatch_queue_t networkConcurrent;

@end

@implementation HTNetworkManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setRequestOperationManager:[[[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.vk.com/method/"]] autorelease]];
    }
    return self;
}

//  id = @"15849064"

#pragma mark - 
#pragma amrk - GetNewsFeed

- (void)getNewsFeedWithAccessToken:(NSString *)accessToken
                       itemsOffset:(NSString *)itemsOffset
                         onSuccess:(NetworkManagerSuccessBlock)success
                         onFailure:(void (^) (NSError *error)) failure
{
    NSDictionary *nextFromDictionary = [NSDictionary dictionaryWithObjectsAndKeys:itemsOffset, @"start_from", nil];
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:accessToken ,@"access_token", @"100", @"count", nextFromDictionary, @"next_from", nil]; //@"next_from"     @"start_from" @"new_from"
    
    __block typeof(self) blockSelf = self;
    [[self requestOperationManager] GET:@"newsfeed.get"
                             parameters:[paramDictionary copy]
                               progress:nil
                                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                                {
                                    success([blockSelf parseNewsWithResponceObject:responseObject]);
                                }
                                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                                {
                                    failure(error);
                                }];
}

#pragma mark -
#pragma mark - GetUserProfiles

- (void)getUserProfilesWithAccessToken:(NSString *)accessToken
                                userId:(NSString *)userId
                             onSuccess:(NetworkManagerSuccessBlock)success
                             onFailure:(void (^) (NSError *error)) failure
{
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:accessToken, @"access_token", userId, @"user_ids", @"verified, sex, bdate, city, country, home_town, photo_200, lists, has_mobile, contacts, site, education, universities, schools, status, followers_count, common_count, occupation, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me", @"fields", @"Nom", @"name_case", nil];
    
    __block typeof(self) blockSelf = self;
    [[self requestOperationManager] GET:@"users.get"
                             parameters:[paramDictionary copy]
                               progress:nil
                                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                                {
                                    success([blockSelf parseProfileWithResponceObject:responseObject]);
                                }
                                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                                {
                                    failure(error);
                                }];
}

#pragma mark - 
#pragma mark - GetUserPhotos

- (void)getUserPhotosWithAccessToken:(NSString *)accessToken
                              userId:(NSString *)userId
                           onSuccess:(NetworkManagerSuccessBlock)success
                           onFailure:(void (^) (NSError *error)) failure
{
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:accessToken, @"access_token", userId, @"owner_id", nil];
    
    __block typeof(self) blockSelf = self;
    [[self requestOperationManager] GET:@"photos.getAll"
                             parameters:[paramDictionary copy]
                               progress:nil
                                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                                {
                                    success([blockSelf parseUserPhotosWithResponceObject:responseObject]);
                                }
                                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                                {
                                    failure(error);
                                }];
}

#pragma mark -
#pragma mark - GetSearchVideo

- (void)getSearchVideoWithAccessToken:(NSString *)accessToken
                                  key:(NSString *)key
                            onSuccess:(NetworkManagerSuccessBlock)success
                            onFailure:(void (^) (NSError *error)) failure
{
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[accessToken retain], @"access_token", [key retain], @"q", @"200", @"count", @"0", @"adult", nil];
    __block typeof(self) blockSelf = self;
    
    [[blockSelf requestOperationManager] GET:@"video.search"
                                  parameters:[paramDictionary retain]
                                    progress:nil
                                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                                        {
                                            success([blockSelf parceSearchVideoWithResponceObject:responseObject]);
                                        }
                                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                                        {
                                            failure(error);
                                        }];
    [accessToken release];
    [key release];
    [paramDictionary release];
}

#pragma mark -
#pragma mark - Parse News

- (NSDictionary *)parseNewsWithResponceObject:(id)responceObject
{
    NSMutableArray *newsArray = [[NSMutableArray alloc] init];
    NSDictionary *responceDictionary = [responceObject objectForKey:@"response"];
    NSArray *userInfo = responceDictionary[@"groups"] ?: responceDictionary[@"user"];
    
    NSString *newOffsetString = [responceDictionary objectForKey:@"new_from"];
//    NSString *newFromString = [responceDictionary objectForKey:@"next_from"];
    
    for (NSInteger indexFriendsCount = 0; indexFriendsCount < [userInfo count]; indexFriendsCount++)
    {
//        NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
        NSDictionary *itemsDictionary = [[responceDictionary objectForKey:@"items"]objectAtIndex:indexFriendsCount];
        NSDictionary *photoDictionary = [[itemsDictionary  objectForKey:@"attachment"] objectForKey:@"photo"];
        NSDictionary *likesDictionary = [itemsDictionary objectForKey:@"likes"];
        NSDictionary *repostDictionary = [itemsDictionary  objectForKey:@"reposts"];
        NSArray *userInfo = responceDictionary[@"groups"] ?: responceDictionary[@"user"];
        
        HTRepost *repostModel = [[[HTRepost alloc] initWithRepostCount:[[repostDictionary objectForKey:@"count"] integerValue]] autorelease];
        
        HTLikes *likesModel = [[[HTLikes alloc] initWithCountLikes:[[likesDictionary objectForKey:@"count"] integerValue]
                                                       userIsLikes:[[likesDictionary objectForKey:@"user_likes"] boolValue]] autorelease];
        
        HTNews *newsModel = [[[HTNews alloc] initWithUserName:[[userInfo objectAtIndex:indexFriendsCount] objectForKey:@"name"]
                                                     postDate:[NSDate dateWithTimeIntervalSince1970:[[itemsDictionary objectForKey:@"date"] doubleValue]]
                                                   likesModel:likesModel
                                                  repostModel:repostModel
                                                 photoUserUrl:[[userInfo objectAtIndex:indexFriendsCount] objectForKey:@"photo"]
                                                  photoPostUr:[photoDictionary objectForKey:@"src_big"] //@"src_small", @"src_big"
                                              descriptionText:[[itemsDictionary objectForKey:@"text"] stringByStrippingHTML]] autorelease];
        
        [newsArray addObject:newsModel];
//        nf[loopPool drain];
    }
    
    HTUser *userModel = [[[HTUser alloc] initWithNews:newsArray
                                          itemsOffset:newOffsetString] autorelease];
    [newsArray release];
    return @{NSStringFromClass([HTUser class]): userModel};
}

#pragma mark - 
#pragma mark - Parse Profile

- (NSDictionary *)parseProfileWithResponceObject:(id)responceObject
{
    NSDictionary *responceDictionary = [[[responceObject objectForKey:@"response"] objectAtIndex:0] retain];
    HTProfile *profileModel = [[HTProfile alloc] initWithFirstName:[responceDictionary objectForKey:@"first_name"]
                                                          lastName:[responceDictionary objectForKey:@"last_name"]
                                                          photoUrl:[responceDictionary objectForKey:@"photo_200"]
                                                       dateOfBirth:[responceDictionary objectForKey:@"bdate"]
                                                        university:[[[responceDictionary objectForKey:@"universities"] objectAtIndex:0] objectForKey:@"name"]
                                                            school:[[[responceDictionary objectForKey:@"schools"] objectAtIndex:0] objectForKey:@"name"]];
    [responceDictionary release];
    return @{NSStringFromClass([HTProfile class]): [profileModel autorelease]};
}

#pragma mark - 
#pragma mark - parseUserPhotos

- (NSDictionary *)parseUserPhotosWithResponceObject:(id)responceObject
{
    NSArray *responceArray = [responceObject objectForKey:@"response"];
    NSMutableArray *photosArray = [[NSMutableArray alloc] init];
    
    for (NSInteger indexResponseCount = 2; indexResponseCount < ([responceArray count] - 2); indexResponseCount++)
    {
        NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
        HTPhoto *photoModel = [[[HTPhoto alloc] initWithPhotoUrl:[[responceArray objectAtIndex:indexResponseCount] objectForKey:@"src_big"]] autorelease];
        [photosArray addObject:photoModel];
        [loopPool drain];
    }
    HTPhotoAlbum *photoAlbum = [[HTPhotoAlbum alloc] initWithPhotosArray:photosArray];
    [photosArray release];
    
    return @{NSStringFromClass([HTPhotoAlbum class]): [photoAlbum autorelease]};
}


#pragma mark -
#pragma mark - parceSearchVideo

- (NSDictionary *)parceSearchVideoWithResponceObject:(id)responceObject
{
    NSArray *responceArray = [responceObject objectForKey:@"response"];
    NSMutableArray *resultArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (NSInteger index = 0; index < [responceArray count]; index++)
    {
        NSDictionary *responceDictionary = [responceArray objectAtIndex:index];
        HTVideoModel *videoModel = [[[HTVideoModel alloc] initWithTitle:[responceDictionary objectForKey:@"title"]
                                                             videoImage:[responceDictionary objectForKey:@"image_medium"]
                                                       descriptionVideo:[[responceDictionary objectForKey:@"description"] stringByStrippingHTML]
                                                       createDateString:[[NSDate dateWithTimeIntervalSince1970:[[responceDictionary objectForKey:@"date"] doubleValue]] converToString]] autorelease];
        [resultArray addObject:[videoModel retain]];
        [videoModel release];
    }
    
    HTSearchVideoModel *searchVideoModel = [[[HTSearchVideoModel alloc] initWithVideoArray:[resultArray retain]] autorelease];
    [resultArray release];
    
    return @{NSStringFromClass([HTSearchVideoModel class]): searchVideoModel};
}

#pragma mark - 
#pragma mark - Dealloc

- (void)dealloc
{
//    [_requestQueue release];
    [_networkConcurrent release];
    [_requestOperationManager release];
    [super dealloc];
}

@end
