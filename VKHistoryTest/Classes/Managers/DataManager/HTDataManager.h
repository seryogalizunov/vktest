//
//  HTDataManager.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/24/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTDataManagerProtocol.h"

@interface HTDataManager : NSObject <HTDataManagerProtocol>


@end
