//
//  HTBaseVIewControllerPotocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/21/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HTBaseViewControllerPotocol <NSObject>

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSString *userId;

- (void)showHud;
- (void)hideHud;

- (__kindof UIViewController *)assemblyViewControllerWithWithIdentifier:(NSString *)identifier;
- (UIStoryboard *)assemblyStoryboard;

@end
