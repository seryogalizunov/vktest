//
//  HTNetworkManagerProtocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkManagerSuccessBlock)(id responseObject);

@protocol HTNetworkManagerProtocol <NSObject>

+ (instancetype)sharedManager;

- (void)getFriendsWithUserId:(NSString *)userId
                       count:(NSInteger)count
                      offset:(NSInteger)offset
                   onSuccess:(void (^) (id friends))success
                   onFailure:(void (^) (NSError *error)) failure;

- (void)getNewsFeedWithAccessToken:(NSString *)accessToken
                       itemsOffset:(NSString *)itemsOffset
                         onSuccess:(NetworkManagerSuccessBlock)success
                         onFailure:(void (^) (NSError *error)) failure;

- (void)getUserProfilesWithAccessToken:(NSString *)accessToken
                                userId:(NSString *)userId
                             onSuccess:(NetworkManagerSuccessBlock)success
                             onFailure:(void (^) (NSError *error)) failure;

- (void)getUserPhotosWithAccessToken:(NSString *)accessToken
                              userId:(NSString *)userId
                           onSuccess:(NetworkManagerSuccessBlock)success
                           onFailure:(void (^) (NSError *error)) failure;

- (void)getSearchVideoWithKey:(NSString *)key
                    onSuccess:(NetworkManagerSuccessBlock)success
                    onFailure:(void (^) (NSError *error)) failure;

@end
