//
//  HTDataManagerProtocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/24/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^DataManagerSuccessBlock)(id responseObject);

@protocol HTDataManagerProtocol <NSObject>

+ (instancetype)sharedManager;

- (void)getNewsFeedWithAccessToken:(NSString *)accessToken
                       itemsOffset:(NSString *)itemsOffset
                         onSuccess:(DataManagerSuccessBlock)success
                         onFailure:(void (^) (NSError *error)) failure;

- (void)getUserProfilesWithAccessToken:(NSString *)accessToken
                                userId:(NSString *)userId
                             onSuccess:(DataManagerSuccessBlock)success
                             onFailure:(void (^) (NSError *error)) failure;

- (void)getUserPhotosWithAccessToken:(NSString *)accessToken
                              userId:(NSString *)userId
                           onSuccess:(DataManagerSuccessBlock)success
                           onFailure:(void (^) (NSError *error)) failure;

- (void)getSearchVideoWithAccessToken:(NSString *)accessToken
                                  key:(NSString *)key
                            onSuccess:(DataManagerSuccessBlock)success
                            onFailure:(void (^) (NSError *error)) failure;

@end
